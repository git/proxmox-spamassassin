include /usr/share/dpkg/default.mk

# howto add rule updates:

# running `make update-sa` updates the core SA rules (in the sa-updates directory)
# running `make update-kam` updates the KAM ruleset (in the kam-updates directory)

PACKAGE = proxmox-spamassassin

OPKGNAME = Mail-SpamAssassin-$(DEB_VERSION_UPSTREAM)

BUILDDIR = $(PACKAGE)-$(DEB_VERSION_UPSTREAM)
ORIG_SRC_TAR=$(PACKAGE)_$(DEB_VERSION_UPSTREAM).orig.tar.gz
DSC = $(PACKAGE)_$(DEB_VERSION_UPSTREAM_REVISION).dsc

DEB = $(PACKAGE)_$(DEB_VERSION_UPSTREAM_REVISION)_amd64.deb
DBG_DEB = $(PACKAGE)-dbgsym_$(DEB_VERSION_UPSTREAM_REVISION)_amd64.deb
DEBS = $(DEB) $(DBG_DEB)

KAM_CHANNEL_FILE = debian/tree/etc/mail/spamassassin/channel.d/KAM_channel.conf
KAM_KEYID = $(shell awk -F '=' '/^KEYID/ { print $$2 }' $(KAM_CHANNEL_FILE))


SA_UPDATE_GPG_DIR = .sa-update-gpghome

deb: $(DEB)

$(DEB) $(DBG_DEB): $(BUILDDIR)
	cd $(BUILDDIR); dpkg-buildpackage -b -us -uc
	lintian $(DEB)

dsc:
	$(MAKE) $(DSC)
	lintian $(DSC)

$(DSC): $(ORIG_SRC_TAR) $(BUILDDIR)
	cd $(BUILDDIR); dpkg-buildpackage -S -us -uc -d

# spamassassin fails running tests as root, since tests starting spamd cause spamd
# to setuid to nobody, resulting in EPERM when spamd tries to access the testdirectory, which is
# used for pid-files and rulesets
.PHONY: sbuild
sbuild: $(DSC)
	sbuild --starting-build-commands 'chmod 775 /build' --starting-build-commands 'chmod 775 %SBUILD_BUILD_DIR' $(DSC)

.PHONY: update-upstream
update-upstream: $(OPKGNAME).tar.gz
	rm -rf upstream
	tar -xzf $<
	mv $(OPKGNAME) upstream

$(ORIG_SRC_TAR): $(BUILDDIR)
	tar czf $(ORIG_SRC_TAR) --exclude="$(BUILDDIR)/debian" $(BUILDDIR)

$(BUILDDIR): sa-updates kam-updates
	rm -rf $@ $@.tmp
	cp -a upstream $@.tmp
	cp -a debian $@.tmp
	mkdir -p $@.tmp/rule-updates/spamassassin
	rsync -av sa-updates/ $@.tmp/rule-updates/spamassassin
	mkdir -p $@.tmp/debian/rule-updates/spamassassin-extra
	rsync -av kam-updates/ $@.tmp/rule-updates/spamassassin-extra
	mv $@.tmp $@

$(OPKGNAME).tar.gz:
	rm -f $@*
	curl -L -o '$@#1.tmp' 'https://www-eu.apache.org/dist/spamassassin/source/$@{,.asc}'
	mv $@.tmp $@
	mv $@.asc.tmp $@.asc
	gpgv --keyring ./sa-releasekey.gpg $@.asc $@

$(SA_UPDATE_GPG_DIR)/.prepared: $(OPKGNAME).tar.gz
	rm -rf $(SA_UPDATE_GPG_DIR)
	mkdir --mode=0700 -p $(SA_UPDATE_GPG_DIR)
	tar --strip-components 2 -xzf $(OPKGNAME).tar.gz $(OPKGNAME)/rules/sa-update-pubkey.txt
	sa-update --gpghomedir $(SA_UPDATE_GPG_DIR) --import sa-update-pubkey.txt
	sa-update --gpghomedir $(SA_UPDATE_GPG_DIR) --import $(KAM_CHANNEL_FILE)
	rm sa-update-pubkey.txt
	touch $@

.PHONY: update-sa
update-sa: $(SA_UPDATE_GPG_DIR)/.prepared
	rm -rf updates.tmp
	sa-update --gpghomedir $(SA_UPDATE_GPG_DIR) --updatedir updates.tmp --channel updates.spamassassin.org
	rsync -av --delete --exclude=local.cf --exclude=regression_tests.cf --exclude=*.txt  --exclude=MIRRORED.BY updates.tmp/updates_spamassassin_org/ sa-updates

.PHONY: update-kam
update-kam: $(SA_UPDATE_GPG_DIR)/.prepared
	rm -rf kam-updates.tmp kam-updates
	sa-update --gpghomedir $(SA_UPDATE_GPG_DIR) --gpgkey $(KAM_KEYID) --updatedir kam-updates.tmp --channel kam.sa-channels.mcgrail.com
	rm -rf kam-updates.tmp/kam_sa-channels_mcgrail_com/MIRRORED.BY
	mv kam-updates.tmp kam-updates

.PHONY: upload
upload: UPLOAD_DIST ?= $(DEB_DISTRIBUTION)
upload: $(DEBS)
	tar cf - $(DEBS) | ssh -X repoman@repo.proxmox.com -- upload --product pmg --dist $(UPLOAD_DIST)

.PHONY: clean distclean
clean:
	rm -rf *.deb $(PACKAGE)-[0-9]*/ *.tmp $(SA_UPDATE_GPG_DIR)
	rm -f *.deb *.dsc *.changes *.build *.buildinfo $(OPKGNAME).tar.gz.* $(PACKAGE)*.tar*


distclean: clean
	rm -rf  $(OPKGNAME).*

.PHONY: dinstall
dinstall: $(DEB)
	dpkg -i $(DEB)
