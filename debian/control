Source: proxmox-spamassassin
Section: perl
Priority: optional
Build-Depends: curl,
               debhelper-compat (= 13),
               gpg,
               libberkeleydb-perl,
               libdbd-sqlite3-perl,
               libdbi-perl,
               libencode-detect-perl,
               libgeo-ip-perl,
               libhtml-parser-perl,
               libio-string-perl,
               netbase,
               libmail-dkim-perl (>= 0.25),
               libmail-dmarc-perl,
               libmail-spf-perl,
               libnet-cidr-lite-perl,
               libnet-dns-perl,
               libnet-libidn2-perl,
               libnetaddr-ip-perl,
               libtext-diff-perl,
               libwww-perl,
               perl (>= 5.8.8-7),
               quilt,
               razor,
               rsync,
               zlib1g-dev,
Maintainer: Proxmox Support Team <support@proxmox.com>
Standards-Version: 4.6.2

Package: proxmox-spamassassin
Architecture: any
Provides: libmail-spamassassin-perl
Replaces: spamassassin, spamc
Conflicts: proxmox-plugin-imageinfo
Breaks: pmg-api (<< 7.2-5)
Depends: curl,
         debconf (>= 1.2.0),
         libarchive-tar-perl,
         libberkeleydb-perl,
         libdbi-perl,
         libencode-detect-perl,
         libgeo-ip-perl,
         libhtml-parser-perl (>= 3.24),
         libhttp-date-perl,
         libio-socket-inet6-perl,
         libio-socket-ssl-perl,
         libio-zlib-perl,
         libmail-dkim-perl (>= 0.25),
         libmail-spf-perl,
         libmailtools-perl,
         libnet-cidr-lite-perl,
         libnet-dns-perl,
         libnet-ident-perl,
         libnetaddr-ip-perl,
         libsocket6-perl,
         libsys-hostname-long-perl,
         libsys-syslog-perl,
         libwww-perl,
         netbase,
         perl (>= 5.8.0),
         razor,
         ${misc:Depends},
         ${perl:Depends},
         ${shlibs:Depends},
Recommends: gpg, libdbd-sqlite3-perl, libmail-dmarc-perl, libnet-libidn2-perl
Description: Spam detector and markup engine
 Mail::SpamAssassin is a module to identify spam using several methods
 including text analysis, internet-based realtime blacklists, statistical
 analysis, and internet-based hashing algorithms.
 .
 Using its rule base, it uses a wide range of heuristic tests on mail headers
 and body text to identify "spam", also known as unsolicited bulk email.
 .
 Once identified, the mail can then be tagged as spam for later filtering
 using the user's own mail user-agent application or at the mail transfer
 agent.
 .
 If you wish to use a command-line filter tool, try the spamassassin
 or the spamd/spamc tools provided.
